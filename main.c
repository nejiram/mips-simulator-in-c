#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define MIPS_REGS 32 // broj registara
#define MEM_DATA_START 0x00000000 // početak dijela memorije za podatke
#define MEM_DATA_SIZE 0x00000100 // veličina dijela memorije za podatke
#define MEM_INSTRUCTIONS_START 0x00000100 // početak dijela memorije za instrukcije
#define MEM_INSTRUCTIONS_SIZE 0x00000080 // veličina dijela memorije za instrukcije

// stanje procesora
typedef struct CPU_State {
  uint32_t PC; // programski brojac
  uint32_t REGS[MIPS_REGS]; // register file
} CPU_State;


// regioni glavne memroije (dio za podatke, dio za instrukcije)
typedef struct {
    uint32_t start, size; // početak regiona i veličina
    uint8_t *mem; // sadržaj
} mem_region;

mem_region MEM_REGIONS[] = {
    {MEM_DATA_START, MEM_DATA_SIZE, NULL},
    {MEM_INSTRUCTIONS_START, MEM_INSTRUCTIONS_SIZE, NULL}
};

#define MEM_NREGIONS 2

// trenutno stanje CPU-a
CPU_State CURRENT_STATE, NEXT_STATE;


// funkcija koja piše u glavnu memoriju
void mem_write(uint32_t address, uint32_t value) {
    int i;
    for (i = 0; i < MEM_NREGIONS; i++) {
        if (address >= MEM_REGIONS[i].start &&
                address < (MEM_REGIONS[i].start + MEM_REGIONS[i].size)) {
            uint32_t offset = address - MEM_REGIONS[i].start;
            // memorija je byte-adresabilna, pa se jedna (32-bitna) riječ "dijeli" na 4 dijela
            MEM_REGIONS[i].mem[offset+3] = (value >> 24) & 0xFF;
            MEM_REGIONS[i].mem[offset+2] = (value >> 16) & 0xFF;
            MEM_REGIONS[i].mem[offset+1] = (value >>  8) & 0xFF;
            MEM_REGIONS[i].mem[offset+0] = (value >>  0) & 0xFF;
            return;
        }
    }
}

// memorija koja čita vrijednost iz glavne memorije
uint32_t mem_read(uint32_t address) {
    int i;
    for (i = 0; i < MEM_NREGIONS; i++) {
        if (address >= MEM_REGIONS[i].start &&
                address < (MEM_REGIONS[i].start + MEM_REGIONS[i].size)) {
            uint32_t offset = address - MEM_REGIONS[i].start;
            // memorija je byte-adresabilna, pa pri čitanju 32-bitne riječi čitaju se 4 byte-a
            return
                (MEM_REGIONS[i].mem[offset+3] << 24) |
                (MEM_REGIONS[i].mem[offset+2] << 16) |
                (MEM_REGIONS[i].mem[offset+1] <<  8) |
                (MEM_REGIONS[i].mem[offset+0] <<  0);
        }
    }

    return 0;
}

// inicijalizacija glavne memorije
void init_memory() {
    int i;
    for (i = 0; i < MEM_NREGIONS; i++) {
        MEM_REGIONS[i].mem = malloc(MEM_REGIONS[i].size);
        memset(MEM_REGIONS[i].mem, 0, MEM_REGIONS[i].size);
    }
}

// funkcija koja učitava program iz .txt datoteke u memoriju instrukcija
void load_program(char *program_filename) {
    FILE *dat_program;
    uint32_t brojac, instr;
    dat_program = fopen(program_filename, "r");
    if (dat_program == NULL) return;
    CURRENT_STATE.PC = MEM_INSTRUCTIONS_START;
    brojac = 0;
    while (fscanf(dat_program, "%x\n", &instr) != EOF) {
        mem_write(MEM_INSTRUCTIONS_START + 4 * brojac, instr);
        //printf("%x\n", mem_read(MEM_INSTRUCTIONS_START + 4 * brojac));
        brojac++;
    }
    fclose(dat_program);
}

// funkcija koja dekodira instrukciju
void decode_instruction(uint32_t instruction) {
    
    
    char *registri[] = {"0", "at", "v0", "v1", "a0", "a1", "a2", "a3",
                        "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7",
                        "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7",
                        "t8", "t9", "k0", "k1", "gp", "sp", "fp", "ra" };
    
    uint32_t pom_instr = instruction;
    uint32_t op = pom_instr >> 26;
    
    //vrijednost op polja je 0 -> r-tip
    if (op == 0){
        uint32_t funct = pom_instr << 26;
        funct = funct >> 26; //function dio instrukcije
        //rs, rt, rd i shamt dio 
        uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        uint32_t rd = (pom_instr >> 11) & 0x1F;
        
        //operacija add
        if (funct == 32) {
            printf("add $%s, $%s, $%s \n", registri[rd], registri[rs], registri[rt]);
        }
        //operacija sub
        else if(funct == 34){
            printf("sub $%s, $%s, $%s \n", registri[rd], registri[rs], registri[rt]);
        }
    }
    
    //lui operacija
    else if (op == 15){
        //uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        int16_t imm = pom_instr & 0xFFFF;
        printf("lui $%s, %d \n", registri[rt], imm);
    }
    //i-tip, lw operacija
    else if (op == 35){
        uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        int16_t imm = pom_instr & 0xFFFF;
        printf("lw $%s, %d($%s) \n", registri[rt], imm, registri[rs]);
    }
    //sw operacija
    else if (op == 43){
        uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        int16_t imm = pom_instr & 0xFFFF;
        printf("sw $%s, %d($%s) \n", registri[rt], imm, registri[rs]);
    }
    else
    printf("Pogrešan opcode! \n");
}

//funkcija koja izvršava instrukciju
void execute_instruction(uint32_t instruction){
    
    uint32_t pom_instr = instruction;
    uint32_t op = pom_instr >> 26;
    
    //vrijednost op polja je 0 -> r-tip
    if (op == 0){
        uint32_t funct = pom_instr << 26;
        funct = funct >> 26; //function dio instrukcije
        //rs, rt, rd i shamt dio 
        uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        uint32_t rd = (pom_instr >> 11) & 0x1F;
        
        //operacija add
        if (funct == 32) {
            NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] + CURRENT_STATE.REGS[rt];
            CURRENT_STATE = NEXT_STATE;
            CURRENT_STATE.PC += 4;
        }
        //operacija sub
        else if(funct == 34){
            NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] - CURRENT_STATE.REGS[rt];
            CURRENT_STATE = NEXT_STATE;
            CURRENT_STATE.PC += 4;
        }
    }
    //i-tip, lui operacija
    else if (op == 15){
        //uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        int16_t imm = pom_instr & 0xFFFF;
        NEXT_STATE.REGS[rt] = imm << 16;
        CURRENT_STATE.PC += 4;
        CURRENT_STATE = NEXT_STATE;
    }
    //i-tip, lw operacija
    else if (op == 35){
        uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        int16_t imm = pom_instr & 0xFFFF;
        NEXT_STATE.REGS[rt] = mem_read(CURRENT_STATE.REGS[rs] + imm);
        CURRENT_STATE = NEXT_STATE;
        CURRENT_STATE.PC += 4;
    }
    //sw operacija
    else if (op == 43){
        uint32_t rs = (pom_instr >> 21) & 0x1F;
        uint32_t rt = (pom_instr >> 16) & 0x1F;
        int16_t imm = pom_instr & 0xFFFF;
        mem_write(CURRENT_STATE.REGS[rs] + imm, CURRENT_STATE.REGS[rt]);
        CURRENT_STATE.PC += 4;
        CURRENT_STATE = NEXT_STATE;
    }
    else
    printf("Pogrešan opcode! \n");
}


int main() {
    init_memory();
    load_program("program.txt"); //popunjavenje memorije instrukcija
    
    printf("Hexadecimalni prikaz instrukcija koje ce se izvrsit: \n");
    uint32_t adresa_instrukcije = MEM_INSTRUCTIONS_START;
    uint32_t instrukcija;
    while(mem_read(adresa_instrukcije) != 0){
        instrukcija = mem_read(adresa_instrukcije);
        printf("%x \n", instrukcija);
        adresa_instrukcije += 4;
    }
    
    printf("Dekodiranje instrukcija: \n");
    adresa_instrukcije = MEM_INSTRUCTIONS_START;
    while(mem_read(adresa_instrukcije) != 0){
        instrukcija = mem_read(adresa_instrukcije);
        decode_instruction(instrukcija);
        adresa_instrukcije += 4;
    }
    
    //popunjavanje memorije podataka
    uint32_t adresa, vrijednost;
    for (adresa = MEM_DATA_START; adresa < MEM_DATA_START + MEM_DATA_SIZE; adresa += 4){
        vrijednost = rand() % 100;
        mem_write(adresa, vrijednost);
    }
    
    printf("Stanje memorije podataka prije izvršenja instrukcija: \n");
    for (adresa = MEM_DATA_START; adresa < MEM_DATA_START + MEM_DATA_SIZE; adresa += 4){
        printf("%d \n", mem_read(adresa));
    }
    
    printf("Stanje registara prije izvrsenja instrukcija: \n");
    int i = 0;
    for (i = 0; i < 32; i++){
        printf("%d \n", CURRENT_STATE.REGS[i]);
    }
    
    //čitanje instrukcija i slanje na izvršavanje
    adresa_instrukcije = MEM_INSTRUCTIONS_START;
    
    while(mem_read(adresa_instrukcije) != 0){
        instrukcija = mem_read(adresa_instrukcije);
        execute_instruction(instrukcija);
        adresa_instrukcije += 4;
    }
    
    printf("Stanje memorije podataka nakon izvršenja instrukcija: \n");
    for (adresa = MEM_DATA_START; adresa < MEM_DATA_START + MEM_DATA_SIZE; adresa += 4){
        printf("%d \n", mem_read(adresa));
    }
    
    printf("Stanje registara nakon izvrsenja instrukcija: \n");
    for (i = 0; i < 32; i++){
        printf("%d \n", CURRENT_STATE.REGS[i]);
    }
    
    
    return 0;
}
